resource "aws_iam_policy" "send_email_policy" {
  name        = var.policy_name
  description = "My test policy"
  policy      = file("policies/TestAmazonSesSendEmail.json")
}
